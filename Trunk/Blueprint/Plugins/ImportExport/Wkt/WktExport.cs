﻿using System;
using Blueprint.ImportExport.Wkt.WktLoader;

namespace Blueprint.Plugins.ImportExport.Wkt
{
	[MenuExtensionUsages("Export", "Well Known Text export...", shortCut = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)]
	public class WktExport : ExportExtensionBase
	{
		public void Execute(IVectorImageProvider imageProvider)
		{
			string wktText = WktWriter.ImageToText(imageProvider.LayeredVectorImage);
			WktParserDialog dlg = new WktParserDialog();
			dlg.WktText = wktText;
			dlg.Show();
		}
	}
}

