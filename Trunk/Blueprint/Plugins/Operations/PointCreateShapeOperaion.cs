﻿using System;
using TrigonometryLib.Primitives;
using System.Collections.Generic;
using Blueprint.Plugins.Operations;
using Blueprint.RenderEngine;
using System.Drawing;

namespace Blueprint
{
	[MenuExtensionUsagesAttribute("Point", "Create shape", shortCut = System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.S)]
	public class PointCreateShapeOperaion : OperationsBase
	{
		public void Execute (IVectorImageProvider imageProvider)
		{
			List<Vector2D> points = imageProvider.CurrImage.GetPrimitives<Vector2D>();

			VectorCloud2D cloud = new VectorCloud2D(points);
			Shape2D shape = new Shape2D(cloud);
			imageProvider.CurrImage.primitives.Add(shape);

			foreach (Vector2D v in cloud)
			{
				imageProvider.CurrImage.primitives.Remove(v);
			}
		}
	}
}

