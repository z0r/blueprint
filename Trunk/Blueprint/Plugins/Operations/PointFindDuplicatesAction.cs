﻿using System;
using TrigonometryLib.Primitives;
using System.Collections.Generic;
using Blueprint.Plugins.Operations;
using Blueprint.RenderEngine;
using System.Drawing;

namespace Blueprint
{
	[MenuExtensionUsagesAttribute("Point", "Highlight duplicates")]
	public class PointFindDuplicatesAction : OperationsBase
	{
		public void Execute (IVectorImageProvider imageProvider)
		{
			VectorCloud2D cloud = new VectorCloud2D();

			List<Line2D> lines = imageProvider.CurrImage.GetPrimitives<Line2D>();
			foreach (Line2D l in lines)
			{
				if (cloud.IndexOf(l.Start) == -1) cloud.Add(l.Start);
				if (cloud.IndexOf(l.End) == -1) cloud.Add(l.End);
			}

			for (int i = 0; i < cloud.Count; ++i)
			{
				for (int j = i + 1; j < cloud.Count; ++j)
				{
					if (cloud[i].IsPositionEqual(cloud[j]))
					{
						PrimitiveRenderData.Get(cloud[i]).Color = Color.Red;
						PrimitiveRenderData.Get(cloud[j]).Color = Color.Red;
					}
				}
			}
		}
	}
}

