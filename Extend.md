### Extending blueprint ###

## Operations ##
There are some mechanisms in place to easily extend blueprint without much effort.
One of these are operations.
These extend the Actions menu in the main menu bar for easy access.
The following class will add a new section under Actions named "My section" and within that the "My operaion" operation.

~~~~
	[MenuExtensionUsages("My section", "My operation")]
	public class MyOperaionOperation : OperationsBase
	{
		public void Execute (IVectorImageProvider imageProvider)
		{
        // Do stuff
		}
	}
~~~~

There are some examples availabe inside the on how to use this, but basicly you get and set data on the imageProvider, which is the vector image that is displayed.
For ease of access, imageProvider has a CurrImage propery to select the current 'layer' which can be set from the UI.

## File support ##
The following class demonstrates on how to extend the import (or export) file functionality. Again in the blueprint code, there are some examples, where CsvImport is probably the easiest to check.
~~~~
    [MenuExtensionUsages("Import", "My text file")]
    public class MyTextFileImporter : ImportExtensionBase
    {
        public void Execute(IVectorImageProvider imageProvider)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "My file (*.txt)|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
            // Do stuff
            }
        }
    }
~~~~